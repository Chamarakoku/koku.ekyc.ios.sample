//
//  ViewController.swift
//  sampleSdkIntergrate
//
//  Created by Mac  on 05/09/19.
//  Copyright © 2019 Think. All rights reserved.
//

import UIKit
import ekyc

class ViewController: UIViewController {
    let sdk = KoKuKYCSDK(client_id: "ekyc.client.01", secret: "41hl9DIYnLPO8rWunArk", subscription: "8710148776e5400a8ef8915a06bf59bc", countryCode: "ID", callBackUrl: "https://stghub.koku.io/api/v1.1/webhook/kyc/ekyc", referenceID: "7bd5f37f-b8df-453e-917e-9dcb83f6f201", enableResultScreen: true, allowImageUploadfromGallery: true, environment: .STAGING)

    override func viewDidLoad() {
        super.viewDidLoad()

        sdk.delegate = self
        
        sdk.setTitleTextColor(color: UIColor.white)
        sdk.setBackgroundColor(color: UIColor(red: 97/255, green: 21/255, blue: 111/255, alpha: 1))
        sdk.setTitleBackgroundColor(color: UIColor(red: 97/255, green: 21/255, blue: 111/255, alpha: 1))
        // Do any additional setup after loading the view, typically from a nib.
    }

    @IBAction func startKycAct(_ sender: Any) {
        sdk.startEkyc()
    }
}

extension ViewController: eKYCResultCallback{
    func onKYCError(kycResult: KYCError) {
        print("kycResult error \(kycResult.error) \n \(kycResult.message)")
    }
    
    func onKYCSuccess(kycResult: KYCResult) {
        print("kycResult \(kycResult.faceComparisonResponse!) \n \(kycResult.livenessResponse!) \n \(kycResult.ocrResponse!)")
    }
}
